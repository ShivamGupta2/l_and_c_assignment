# IMQ:

1. Messaging Queue system

### 1. Setup
#### 1.1 Installation

 Run `npm install`

#### 1.2 Compilation

1. Run `gulp` inside server folder
2. Run `gulp` inside client folder

#### 1.3 Running Server

1. Go into server/dist folder
2. Run `node index.js`

### 1.4 Running Client

1. Go into client/dist folder
2. Run `node index.js`

