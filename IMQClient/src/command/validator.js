const InvalidCommand = require("../exception/command")

class CommandValidator {
    static optionsValidator(options) {
        for (let option in options) {
            if (options[option] == undefined || options[option].trim() == "" || options[option].trim()[0] == "-")
                throw new InvalidCommand("invalid command")
        }
    }

    static numberOfOptionValidator(noOfOptions, options) {
        if (Object.keys(options).length != noOfOptions)
            throw new InvalidCommand("invalid command")
    }
}

module.exports = CommandValidator