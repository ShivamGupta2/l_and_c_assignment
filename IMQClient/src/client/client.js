let config = require("../config.json")
const net = require('net')
const logger = require("../logger/logger")
const responseType = require("../../../IMQProtocol/type/type")
const ClientService = require("../service/clientService")
const rl = require("../consoleInteraction/readline")
const publisherService = require("../service/publisher/publisherService")
const subscriberService = require("../service/subscriber/subscriberService")
const ResponseError = require("../exception/response")

class Client extends ClientService {
    constructor() {
        super()
        this.createSocket()
        this.connectToServer()
        this.clientListner()
    }

    createSocket() {
        this.client = new net.Socket();
    }

    connectToServer() {
        this.client.connect(config.port, config.host, () => this.connectEvent())
    }

    clientListner() {
        this.client.on('data', data => {
            this.dataEvent(data)
        })
        this.client.on('error', error => this.errorEvent(error))
        this.client.on("end", () => {
            logger.log("connection closed")
            process.exit();
        })
    }

    async dataEvent(response) {
        response = this.parseResponse(response)
        if (!response.headers.error) {
            switch (response.headers.responseType) {
                case responseType.register:
                    logger.log(response.body.data)
                    this.closeConnection()
                    break
                case responseType.logIn:
                    logger.log(response.body.data)
                    this.getAllTopics()
                    break
                case responseType.topic:
                    this.showAllTopics(response.body.data)
                    this.topic = await rl.getInputFromConsole("choose topic from above list")
                    logger.log("connecting to topic :- " + this.topic)
                    if (this.type == "publisher") {
                        let message = await publisherService.askForMessage()
                        publisherService.pushMesage({
                            message,
                            topic: this.topic,
                            user: this.userName
                        }, this.client)
                    } else if (this.type == "subscriber") {
                        subscriberService.pullMessages({
                            topic: this.topic,
                            user: this.userName
                        }, this.client)
                    } else {
                        logger.log("invalid user closing connection")
                        this.closeConnection()
                    }
                    break
                case responseType.push:
                    logger.log(response.body.data)
                    if (await rl.getInputFromConsole("Write 1 if you want to send more messages") == 1) {
                        this.getAllTopics()
                    } else
                        this.closeConnection()
                    break
                case responseType.pull:
                    subscriberService.showMessages(response.body.data, this.client)
                    this.closeConnection()
                    break
            }
        } else {
            logger.log(`Error: ${response.body.error}`)
            this.closeConnection()
        }
    }

    errorEvent(error, client) {
        logger.log(`server disconnected :- ${error.message}`)
    }

    connectEvent() {
        logger.log("connected to server")
    }

    closeConnection() {
        this.client.end();
    }

    register(user) {
        super.register(user, this.client)
    }

    logIn(user) {
        this.userName = user.name
        this.type = user.type
        super.logIn(user, this.client)
    }

    getAllTopics() {
        super.getAllTopics(this.client)
    }

    parseResponse(response) {
        try {
            return JSON.parse(response);
        } catch (error) {
            new ResponseError.ParseError()
        }
    }
}

module.exports = Client