const log = logMessage => console.log(logMessage)

const logClientInfo = socket => {
    log('------------remote client info --------------');
    let remotePort = socket.remotePort;
    let remoteAddress = socket.remoteAddress;
    log('REMOTE Socket is listening at port :' + remotePort);
    log('REMOTE Socket ip :' + remoteAddress);
}

module.exports = {
    log,
    logClientInfo
}