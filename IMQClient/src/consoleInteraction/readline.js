const readline = require("readline")

class Readline {

  async getInputFromConsole(query) {
    let readLine = this.getRealineObject();
    return new Promise((resolve) =>
      readLine.question(`${query}\n`, (ans) => {
        resolve(ans);
        readLine.close();
      })
    );
  }

  getRealineObject() {
    const readLine = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
      terminal: false,
    });
    return readLine;
  }
}

module.exports = new Readline();