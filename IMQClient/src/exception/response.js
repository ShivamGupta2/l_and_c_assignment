class ParseError extends Error {
    constructor() {
        super("Error in parsing response \n please restart the application")
    }
}

class ResponseError extends Error {
    constructor(error) {
        super(error)
    }
}

module.exports = {
    ParseError,
    ResponseError
}