module.exports = class InvalidCommand extends Error{
    constructor() {
        super("Invalid command entered")
    }
}