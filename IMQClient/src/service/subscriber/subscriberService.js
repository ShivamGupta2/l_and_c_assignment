const RequestBuilder = require("../../../../IMQProtocol/request/builder")
const logger = require("../../logger/logger")

class SubscriberService {
    pullMessages(topic, client) {
        logger.log("pulling message from server")
        client.write(RequestBuilder.buildJSONRequest(topic, "pull"))
    }
    
    showMessages(response) {
        if(response.length){
            response.map(message=>{
                logger.log(`${message.createdDate} : ${message.message}`)
            })
        }
        else
            logger.log("no message to display")
    }
}

module.exports = new SubscriberService()