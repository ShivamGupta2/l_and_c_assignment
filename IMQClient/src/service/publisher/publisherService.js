const RequestBuilder = require("../../../../IMQProtocol/request/builder")
const logger = require("../../logger/logger")
const rl = require("../../consoleInteraction/readline")

class PublisherService {
    async askForMessage() {
        let message = await rl.getInputFromConsole("type message you want to push")
        return message
    }
    
    pushMesage(message, client) {
        logger.log("pushing message to the server")
        client.write(RequestBuilder.buildJSONRequest(message, "push"))
    }
}

module.exports = new PublisherService()