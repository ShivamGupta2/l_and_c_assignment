const RequestBuilder = require("../../../IMQProtocol/request/builder")
const logger = require("../logger/logger")

class ClientService {
    register(user, client) {
        client.write(RequestBuilder.buildJSONRequest(user, "register"))
    }

    logIn(user, client) {
        client.write(RequestBuilder.buildJSONRequest(user, "login"))
    }

    getAllTopics(client) {
        client.write(RequestBuilder.buildJSONRequest("", "topic"))
    }
    
    showAllTopics(topicObjectArray) {
        let topics = []
        for (let topicObject of topicObjectArray)
            topics.push(topicObject.name)
        logger.log(topics)
    }
}

module.exports = ClientService