#!/usr/bin/env node

const commander = require('commander');
const package = require("../package.json")
const Client = require("../src/client/client")
const CommandValidator = require("../src/command/validator")
const logger = require("../src/logger/logger")

const app = new commander.Command()
    .option(`-v, ${package.version}`);

app.command('register')
    .option('-n --name <string>', 'username or email address')
    .option('-p --password <string>', 'password for login')
    .option('-t --type <string>', 'user type publisher/subscriber')
    .action((options, command) => {
        try {
            CommandValidator.numberOfOptionValidator(3, options)
            CommandValidator.optionsValidator(options)
            let client = new Client().register(options)
        } catch (error) {
            actionErrorHanlder(error)
        }
    });

app.command("publish")
    .option('-n, --name <string>', 'username or email address')
    .option('-p --password <string>', 'password for login')
    .action((options, command) => {
        try {
            CommandValidator.numberOfOptionValidator(2, options)
            CommandValidator.optionsValidator(options)
            options.type = "publisher"
            let client = new Client().logIn(options)
        } catch (error) {
            actionErrorHanlder(error)
        }
    });

app.command("subscribe")
    .option('-n, --name <string>', 'username or email address')
    .option('-p --password <string>', 'password for login')
    .action((options, command) => {
        try {
            CommandValidator.numberOfOptionValidator(2, options)
            CommandValidator.optionsValidator(options)
            options.type = "subscriber"
            let client = new Client().logIn(options)
        } catch (error) {
            actionErrorHanlder(erro)
        }
    });

app.parse()

function actionErrorHanlder(error) {
    logger.log(error.message);
    logger.log("please use help option\n imq-cli -h or imq-cli commandName -h")
}