const gulp = require("gulp");
const terser = require('gulp-terser');

function json() {
    return gulp.src(["src/**/*.json",'!node_modules/**'])
        .pipe(gulp.dest("dist/"))
}

function js() {

    return gulp.src(['src/**/*.js', "!gulpfile.js", '!node_modules/**'])
        .pipe(terser())
        .pipe(gulp.dest("dist/"));
}
exports.default = gulp.series(json, js);