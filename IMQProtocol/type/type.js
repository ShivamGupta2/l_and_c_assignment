const type = {
    register: "register",
    logIn: "login",
    push: "push",
    pull: "pull",
    topic: "topic"
}
Object.freeze(type)

module.exports = type