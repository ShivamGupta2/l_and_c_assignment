class RequestBuilder {
    static buildJSONRequest(data, requestType) {
        return JSON.stringify({
            headers: {
                containtType: "JSON",
                requestType: requestType
            },
            body: this.buildBody(data)
        })
    }

    static buildBody(data) {
        let date = new Date()
        return {
            createdDate: `${date.toLocaleDateString()} ${date.toLocaleTimeString()}`,
            data: data,
            size: data.length
        }
    }
}

module.exports = RequestBuilder