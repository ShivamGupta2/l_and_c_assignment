class ResponseBuilder {
    static buildJSONResponse(responseType, data) {
        return JSON.stringify({
            headers: {
                containtType: "JSON",
                responseType: responseType,
                error: false
            },
            body: {
                data: data
            }
        })
    }

    static buildJSONErrorResponse(responseType, error) {
        return JSON.stringify({
            headers: {
                containtType: "JSON",
                responseType: responseType,
                error: true
            },
            body: {
                error: error
            }
        })
    }
}

module.exports = ResponseBuilder