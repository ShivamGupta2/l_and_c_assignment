let mongodb = require("mongodb");

class Database {
    constructor(url) {
        this.url = url
    }

    async databaseConnection() {
        this.client = await new mongodb.MongoClient(this.url, {
            useUnifiedTopology: true
        });
        await this.client.connect();
    }

    async find(dbName, collectionName, query = {}) {
        const db = this.client.db(dbName);
        const collection = db.collection(collectionName)
        let results = await collection.find(query).toArray();
        return results
    }

    async insert(dbName, collectionName, document) {
        const db = this.client.db(dbName);
        const collection = db.collection(collectionName)
        return await collection.insertOne(document);
    }

    async update(dbName, collectionName, query, options) {
        const db = this.client.db(dbName);
        const collection = db.collection(collectionName)
        await collection.updateOne(query, options)
    }

    async closeConnection() {
        await this.client.close()
    }
}

module.exports = Database