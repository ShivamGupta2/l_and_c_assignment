const Database = require("../database")
const config = require("../../util/config.json")
const topicException = require("../../exception/topic")

class TopicDbLayer {
    static async getAllTopics() {
        let db = new Database(config.dbUrl);
        return new Promise((resolve, reject) => {
            db.databaseConnection()
                .then(() => db.find(config.dbName, config.topicCollectionName))
                .then(response => {
                    db.closeConnection()
                    if (response.length == 0) {
                        reject(new topicException.NotExists())
                    } else {
                        resolve(response)
                    }
                })
                .catch(error => reject(error))
        })
    }

    static async getTopic(topic) {
        let db = new Database(config.dbUrl);
        return new Promise((resolve, reject) => {
            db.databaseConnection()
                .then(() => db.find(config.dbName, config.topicCollectionName, {
                    name: topic
                }))
                .then(response => {
                    db.closeConnection()
                    if (response.length == 0) {
                        reject(new topicException.NotExists())
                    } else {
                        resolve(response)
                    }
                })
        })
    }

    static async updateLastReadMessageId(topic, user, index) {
        let db = new Database(config.dbUrl);
        let query, options
        try {
            query = {
                name: topic,
                "subscribers.userName": user
            }
            options = {
                $inc: {
                    "subscribers.$.lastReadMessageId": index
                }
            }
        } catch (error) {
            throw new topicException.InvalidUpdate()
        }
        return new Promise((resolve, reject) => {
            db.databaseConnection()
                .then(() => db.update(config.dbName, config.topicCollectionName, query, options))
                .then(() => resolve())
                .catch(error => reject(error))
        })
    }

    static async addSubscriber(topic, user) {
        let db = new Database(config.dbUrl);
        let query, options
        try {
            query = {
                name: topic
            }
            options = {
                $push: {
                    subscribers: {
                        lastReadMessageId: 0,
                        userName: user
                    }
                }
            }
        } catch (error) {
            throw new topicException.InvalidUpdate()
        }
        return new Promise((resolve, reject) => {
            db.databaseConnection()
                .then(() => db.update(config.dbName, config.topicCollectionName, query, options))
                .then(() => resolve({
                    message: "successfully added as subscriber to topic"
                }))
                .catch(error => reject(error))
        })
    }

    static async addPublisher(topic, user) {
        let db = new Database(config.dbUrl);
        let query, options
        try {
            query = {
                name: topic
            }
            options = {
                $push: {
                    publishers: user
                }
            }
        } catch (error) {
            throw new topicException.InvalidUpdate()
        }
        return new Promise((resolve, reject) => {
            db.databaseConnection()
                .then(() => db.update(config.dbName, config.topicCollectionName, query, options))
                .then(() => resolve({
                    message: "successfully added as publisher to topic"
                }))
                .catch(error => reject(error))
        })
    }

    static async createTopic(topic) {
        let db = new Database(config.dbUrl);
        let document = {
            "name": topic,
            "subscribers": [],
            "publishers": []
        }
        return new Promise((resolve, reject) => {
            db.databaseConnection()
                .then(() => db.insert(config.dbName, config.topicCollectionName, document))
                .then(() => resolve({
                    message: "successfully created topic"
                }))
                .catch(error => reject(error))
        })
    }
}

module.exports = TopicDbLayer