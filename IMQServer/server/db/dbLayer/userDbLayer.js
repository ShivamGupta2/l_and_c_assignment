const Database = require("../database")
const config = require("../../util/config.json")
const RegistrationException = require("../../exception/register")
const LogInException = require("../../exception/login")

class UserDbLayer {
    static async register(user) {
        let db = new Database(config.dbUrl);
        return new Promise((resolve, reject) => {
            db.databaseConnection()
                .then(() => db.find(config.dbName, config.userCollectionName, {
                    name: user.name
                }))
                .then(response => {
                    if (response.length != 0) {
                        reject(new RegistrationException.UserExists())
                    } else
                        db.insert(config.dbName, config.userCollectionName, user)
                        .then(response => {
                            db.closeConnection()
                            resolve(response.ops[0])
                        })
                        .catch(error => reject(error))
                })
        })
    }

    static async logIn(user) {
        let db = new Database(config.dbUrl);
        return new Promise((resolve, reject) => {
            db.databaseConnection()
                .then(() => db.find(config.dbName, config.userCollectionName, user))
                .then(response => {
                    if (response.length == 0) {
                        reject(new LogInException.UserNotExists())
                    } else {
                        db.closeConnection()
                        resolve(response)
                    }
                })
                .catch(error => reject(error))
        })
    }
}

module.exports = UserDbLayer