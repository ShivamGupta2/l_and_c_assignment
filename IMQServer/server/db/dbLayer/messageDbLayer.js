const Database = require("../database")
const config = require("../../util/config.json")
const language = require("../../language.json")
const messageException = require("../../exception/message")

class MessageDbLayer {
    static async getMessages(topic) {
        let db = new Database(config.dbUrl);
        return new Promise((resolve, reject) => {
            db.databaseConnection()
                .then(() => db.find(config.dbName, config.messageCollectionName, {
                    topic: topic
                }))
                .then(response => {
                    db.closeConnection()
                    if (response.length == 0) {
                        reject(new messageException.NoMessagePbulished())
                    } else {
                        resolve(response)
                    }
                })
                .catch(error => reject(error))
        })
    }

    static async pushMessages(body, topic) {
        let db = new Database(config.dbUrl);
        let query, options
        try {
            query = {
                topic: topic
            }
            options = {
                $push: {
                    messages: {
                        createdDate: body.createdDate,
                        message: body.data.message,
                        userName: body.data.user
                    }
                }
            }
        } catch (error) {
            throw new messageException.InvalidMessage()
        }
        return new Promise((resolve, reject) => {
            db.databaseConnection()
                .then(() => db.update(config.dbName, config.messageCollectionName, query, options))
                .then(() => resolve({
                    message: language.savedMessages
                }))
                .catch(error => reject(error))
        })
    }

    static async createMessageDocument(topic) {
        let db = new Database(config.dbUrl);
        let document = {
            "topic": topic,
            "messages": []
        }
        return new Promise((resolve, reject) => {
            db.databaseConnection()
                .then(() => db.insert(config.dbName, config.messageCollectionName, document))
                .then(() => resolve({
                    message: "successfully created message Object in db"
                }))
                .catch(error => reject(error))
        })
    }
}

module.exports = MessageDbLayer