const assert = require("chai").assert
const mocha = require("mocha")
const topicDbLayer = require("../../../db/dbLayer/topicDbLayer")
const language = require("../../../language.json")

describe("get all topics", () => {
    it("return all topics in db", done => {
        topicDbLayer.getAllTopics()
            .then(response => {
                assert.isArray(response)
                assert.property(response[0], "name")
                assert.property(response[0], "subscribers")
                assert.property(response[0], "publishers")
                done()
            })
    })
})

describe("get topics", () => {
    it("return topic", done => {
        let topic = "history"
        topicDbLayer.getTopic(topic)
            .then(response => {
                assert.isArray(response)
                assert.property(response[0], "name")
                assert.property(response[0], "subscribers")
                assert.property(response[0], "publishers")
                assert.equal(response[0].name, topic)
                done()
            })
    })

    it("should throw error no topic exists", done => {
        let topic = "science"
        topicDbLayer.getTopic(topic)
            .then(response => {
                done()
            })
            .catch(error => {
                assert.equal(error.message, language.errors.NO_TOPIC)
                done()
            })
    })
})