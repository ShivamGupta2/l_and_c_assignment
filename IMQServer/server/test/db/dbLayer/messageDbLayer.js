const assert = require("chai").assert
const mocha = require("mocha")
const messageDbLayer = require("../../../db/dbLayer/messageDbLayer")
const language = require("../../../language.json")

describe("message db layer tests for get messages", () => {
    it("should return messages in db", done => {
        let topic = "history"
        messageDbLayer.getMessages(topic)
            .then(response => {
                assert.equal(response[0].topic, topic)
                assert.isArray(response[0].messages)
                done()
            })
    })

    it("should throw error no messages exists", done => {
        let topic = "science"
        messageDbLayer.getMessages(topic)
            .then(response => {
                done()
            })
            .catch(error => {
                assert.equal(error.message, language.errors.NO_MESSAGE)
                done()
            })
    })
})

describe("message db layer tests for push message", () => {
    it("should save message in db", done => {
        let topic = "history"
        let body = {
            createdDate: new Date().toString(),
            data: {
                message: "test string",
                userName: "test"
            }
        }
        messageDbLayer.pushMessages(body, topic)
            .then(response => {
                assert.equal(response.message.topic, 'saved Successfully')
                done()
            })
            .catch(error => {
                done()
            })
    })
})