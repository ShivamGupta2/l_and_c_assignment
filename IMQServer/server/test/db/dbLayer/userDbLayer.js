const assert = require("chai").assert
const mocha = require("mocha")
const userDbLayer = require("../../../db/dbLayer/userDbLayer")
const language = require("../../../language.json")

describe('Register User', () => {
    it("Register new user as publisher", done => {
        const user = {
            name: "Vikas",
            password: "myPassword",
            type: "publisher"
        }
        userDbLayer.register(user)
            .then(response => {
                assert.equal(response.name, user.name)
                assert.equal(response.type, user.type)
                done()
            })
    })

    it("Register new user as publisher", done => {
        const user = {
            name: "Vikas1",
            password: "myPassword",
            type: "subscriber"
        }
        userDbLayer.register(user)
            .then(response => {
                assert.equal(response.name, user.name)
                assert.equal(response.type, user.type)
                done()
            })
    })

    it("Should throw an error User exists", done => {
        const user = {
            name: "Vikas",
            password: "myPassword",
            type: "publisher"
        }
        userDbLayer.register(user)
            .then(response => {
                done()
            })
            .catch(error => {
                assert.equal(error.message, language.errors.REGISTER)
                done()
            })
    })

    it("Should throw an error User exists", done => {
        const user = {
            name: "Vikas1",
            password: "myPassword",
            type: "subscriber"
        }
        userDbLayer.register(user)
            .then(response => {
                done()
            })
            .catch(error => {
                assert.equal(error.message, language.errors.REGISTER)
                done()
            })
    })
})

describe('Login User', () => {
    it("User logged in successfully", done => {
        const user = {
            name: "Vikas",
            password: "myPassword",
            type: "publisher"
        }
        userDbLayer.logIn(user)
            .then(response => {
                assert.equal(response[0].name, user.name)
                assert.equal(response[0].type, user.type)
                done()
            })
    })

    it("should throw an error", done => {
        const user = {
            name: "Vikas",
            password: "myPassword",
            type: "subscriber"
        }
        userDbLayer.logIn(user)
            .then(response => {
                done()
            })
            .catch(error => {
                assert.equal(error.message, language.errors.LOGIN)
                done()
            })
    })

    it("User logged in successfully", done => {
        const user = {
            name: "Vikas1",
            password: "myPassword",
            type: "subscriber"
        }
        userDbLayer.logIn(user)
            .then(response => {
                assert.equal(response[0].name, user.name)
                assert.equal(response[0].type, user.type)
                done()
            })
    })

    it("should throw an error", done => {
        const user = {
            name: "Vikas1",
            password: "myPassword",
            type: "publisher"
        }
        userDbLayer.logIn(user)
            .then(response => {
                done()
            })
            .catch(error => {
                assert.equal(error.message, language.errors.LOGIN)
                done()
            })
    })

    it("should throw an error", done => {
        const user = {
            name: "Vikas112",
            password: "myPassword",
            type: "publisher"
        }
        userDbLayer.logIn(user)
            .then(response => {
                done()
            })
            .catch(error => {
                assert.equal(error.message, language.errors.LOGIN)
                done()
            })
    })
})