const validator = require("../../validator/request")
const assert = require("chai").assert
const mocha = require("mocha")
const language = require("../../language.json")

describe("validate pull request", () => {
    it("test should pass", (done) => {
        let topic = [{
            "name": "history",
            "subscribers": [{
                "lastReadMessageId": 0,
                "userName": "shivam1"
            }],
            "publishers": [
                "shivam"
            ]
        }]
        let user = "shivam1"
        validator.validatePullRequest(topic, user)
        done()
    })

    it("test should fail with error User is not subscribed to topic", (done) => {
        let topic = [{
            "name": "history",
            "subscribers": [{
                "lastReadMessageId": 0,
                "userName": "shivam1"
            }],
            "publishers": [
                "shivam"
            ]
        }]
        let user = "shivam"
        assert.throw(() => validator.validatePullRequest(topic, user), Error, "User is not subscribed to topic")
        done()
    })
})

describe("validate push request", () => {
    it("test should pass", (done) => {
        let topic = [{
            "name": "history",
            "subscribers": [{
                "lastReadMessageId": 0,
                "userName": "shivam1"
            }],
            "publishers": [
                "shivam"
            ]
        }]
        let user = "shivam"
        validator.validatePushRequest(topic, user)
        done()
    })

    it("test should fail with error User is not subscribed to topic", (done) => {
        let topic = [{
            "name": "history",
            "subscribers": [{
                "lastReadMessageId": 0,
                "userName": "shivam1"
            }],
            "publishers": [
                "shivam"
            ]
        }]
        let user = "shivam1"
        assert.throw(() => validator.validatePushRequest(topic, user), Error, language.errors.USER_NOT_SUBSCRIBED)
        done()
    })
})