const assert = require("chai").assert
const mocha = require("mocha")
const Queue = require("../../queue/queue")

describe('Queue', () => {
    let queue;

    beforeEach(() => {
        queue = new Queue();
    });

    it('starts empty', () => {
        assert.equal(queue.size(), 0)
        assert.equal(queue.isEmpty(), true)
    });

    it('enqueues elements', () => {
        queue.enqueue(1);
        assert.equal(queue.size(), 1)
        queue.enqueue(2);
        assert.equal(queue.size(), 2)
        queue.enqueue(3);
        assert.equal(queue.size(), 3)
        assert.equal(queue.isEmpty(), false)
    });

    it('dequeue elements', () => {
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        assert.equal(queue.dequeue(), 1)
        assert.equal(queue.dequeue(), 2)
        assert.equal(queue.dequeue(), 3)
        assert.equal(queue.dequeue(), "Underflow")
    });

    it('implements FIFO logic', () => {
        queue.enqueue(1);
        assert.equal(queue.peek(), 1)
        queue.enqueue(2);
        assert.equal(queue.peek(), 1)
        queue.enqueue(3);
        assert.equal(queue.peek(), 1)

        assert.equal(queue.dequeue(), 1);
        assert.equal(queue.dequeue(), 2);
        assert.equal(queue.dequeue(), 3);
        assert.equal(queue.dequeue(), "Underflow");
    });

    it('allows to peek at the front element in the queue without dequeuing it', () => {
        assert.equal(queue.peek(), "Underflow")

        queue.enqueue(1);
        assert.equal(queue.peek(), 1)

        queue.enqueue(2);
        assert.equal(queue.peek(), 1)

        queue.dequeue();
        assert.equal(queue.peek(), 2)
    });

    it('returns the correct size', () => {
        assert.equal(queue.size(), 0)
        queue.enqueue(1);
        assert.equal(queue.size(), 1)
        queue.enqueue(2);
        assert.equal(queue.size(), 2)
        queue.enqueue(3);
        assert.equal(queue.size(), 3)

        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        assert.equal(queue.size(), 6)

        queue.dequeue();
        assert.equal(queue.size(), 5)
        queue.dequeue();
        assert.equal(queue.size(), 4)
        queue.dequeue();
        assert.equal(queue.size(), 3)
        queue.dequeue();
        assert.equal(queue.size(), 2)
    });

    it('returns if it is empty', () => {
        assert.equal(queue.isEmpty(), true)
        queue.enqueue(1);
        assert.equal(queue.isEmpty(), false)
        queue.enqueue(2);
        assert.equal(queue.isEmpty(), false)
        queue.enqueue(3);
        assert.equal(queue.isEmpty(), false)

        queue.dequeue();
        assert.equal(queue.isEmpty(), false)
        queue.dequeue();
        assert.equal(queue.isEmpty(), false)
        queue.dequeue();
        assert.equal(queue.isEmpty(), true)
        queue.dequeue();
        assert.equal(queue.isEmpty(), true)
    });
});