const language = require("../language.json")

class UserExists extends Error {
    constructor() {
        super(language.errors.REGISTER)
    }
}

module.exports = {
    UserExists
}