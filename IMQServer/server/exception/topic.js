const language = require("../language.json")

class NotExists extends Error {
    constructor() {
        super(language.errors.NO_TOPIC)
    }
}

class InvalidUpdate extends Error {
    constructor() {
        super(language.errors.INVALID_TOPIC_UPDATE)
    }
}

module.exports = {
    NotExists,
    InvalidUpdate
}