const language = require("../language.json")

class NoMessagePbulished extends Error {
    constructor() {
        super(language.errors.NO_MESSAGE)
    }
}

class InvalidMessage extends Error {
    constructor() {
        super(language.errors.INVALID_MESSAGE)
    }
}

module.exports = {
    NoMessagePbulished,
    InvalidMessage
}