const language = require("../language.json")

class UserNotExists extends Error {
    constructor() {
        super(language.errors.LOGIN)
    }
}

module.exports = {
    UserNotExists
}