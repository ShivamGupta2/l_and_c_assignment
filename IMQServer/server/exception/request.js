const language = require("../language.json")

class InvalidRequest extends Error {
    constructor() {
        super(language.errors.INVALID_REQUEST)
    }
}

class NoSubscriber extends Error {
    constructor() {
        super(language.errors.NO_SUBSCRIBER)
    }
}

class UserNotSubscribed extends Error {
    constructor() {
        super(language.errors.USER_NOT_SUBSCRIBED)
    }
}

class NoPublisher extends Error {
    constructor() {
        super(language.errors.NO_PUBLISHER)
    }
}

class UserNotPublisher extends Error {
    constructor() {
        super(language.errors.USER_NOT_SUBSCRIBED)
    }
}

module.exports = {
    InvalidRequest,
    NoSubscriber,
    UserNotSubscribed,
    NoPublisher,
    UserNotPublisher
}