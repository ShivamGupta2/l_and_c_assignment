const logger = require("./logger/logger")
const socketEvent = require("./socketEvent")
const language = require("./language.json")

const connectionEvent = socket => {
    logger.logClientInfo(socket);

    socket.on('data', message => socketEvent.dataEvent(socket, message));

    socket.on('error', error => socketEvent.errorEvent(socket, error));

    socket.on('end', () => socketEvent.endEvent(socket));
}

const errorEvent = error => {
    logger.error('Error: ' + error.message
)};

const closeEvent = () => logger.log(language.serverClose);

module.exports = {
    connectionEvent,
    errorEvent,
    closeEvent
}