const ResponseBuilder = require("../../../IMQProtocol/response/builder")

const acknowledgeClient = (socket, responseType, message) => {
    socket.write(ResponseBuilder.buildJSONResponse(responseType, message))
}

module.exports = acknowledgeClient