let net = require('net');
const serverEvent = require("./serverEvent");
const config = require("./util/config.json");

let server = net.createServer();

server.on('close', () => serverEvent.closeEvent());

server.on('error', error => serverEvent.errorEvent(error));

server.on('connection', socket => serverEvent.connectionEvent(socket));

server.listen(config.port)