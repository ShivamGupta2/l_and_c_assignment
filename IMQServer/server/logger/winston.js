const { format } = require("winston");
const winston = require("winston");
const winstonLogger = winston.createLogger({
    format: format.combine(
        format.errors({
            stack: true
        }),
        format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        winston.format.simple(),
    ),
    transports: [
        new winston.transports.File({
            filename: 'error.log',
            level: 'error'
        }),
        new winston.transports.File({
            filename: 'combined.log'
        }),
    ],
})

module.exports = winstonLogger