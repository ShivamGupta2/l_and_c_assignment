const winstonLogger = require("./winston")

class Logger {

    log(logMessage) {
        winstonLogger.info(logMessage)
        console.log(String(logMessage))
    }

    logClientInfo(socket) {
        winstonLogger.info('------------remote client info --------------');
        console.log('------------remote client info --------------');
        let remotePort = socket.remotePort;
        let remoteAddress = socket.remoteAddress;
        winstonLogger.info('REMOTE Socket is listening at port :' + remotePort);
        winstonLogger.info('REMOTE Socket ip :' + remoteAddress);
        console.log('REMOTE Socket is listening at port :' + remotePort);
        console.log('REMOTE Socket ip :' + remoteAddress);
    }

    error(error) {
        winstonLogger.error(error)
        console.error(error)
    }
}

module.exports = new Logger()