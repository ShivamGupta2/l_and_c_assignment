const TopicDbLayer = require("../db/dbLayer/topicDbLayer")

class QueueTopicSync {
    getLastReadMessageIndex(topic, user) {
        return topic[0].subscribers.filter(subscriber => subscriber.userName == user)[0].lastReadMessageId
    }

    async incrementLastReadMessageIndex(topic, user, index) {
        await TopicDbLayer.updateLastReadMessageId(topic, user, index)
    }
}

module.exports = new QueueTopicSync()