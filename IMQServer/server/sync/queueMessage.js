const MessageDbLayer = require("../db/dbLayer/messageDbLayer")

class QueueMessageSync {
    async push(request, queue) {
        await MessageDbLayer.pushMessages(request.body, request.body.data.topic)
        queue.enqueue({
            messages: {
                createdDate: request.body.createdDate,
                message: request.body.data.message,
                userName: request.body.data.user
            }
        })
    }

    pull(queue, index) {
        return queue.pull(index)
    }
}

module.exports = new QueueMessageSync()