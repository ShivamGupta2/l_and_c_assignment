let logger = require('./logger/logger')
const requestType = require("../../IMQProtocol/type/type")
const acknowledgeClient = require("./util/acknowledgeClient")
const responseMessage = require("../../IMQProtocol/response/message.json")
const ResponseBuilder = require("../../IMQProtocol/response/builder")
const UserDbLayer = require("./db/dbLayer/userDbLayer")
const TopicDbLayer = require("./db/dbLayer/topicDbLayer")
const MessageDbLayer = require('./db/dbLayer/messageDbLayer')
const RequestValidator = require("./validator/request")
const RequestException = require("./exception/request")
const QueueHandler = require("./queue/queueHandler")
const QueueMessageSync = require("./sync/queueMessage")
const QueueTopicSync = require("./sync/queueTopic")

const dataEvent = async (socket, request) => {
    logger.logClientInfo(socket);
    logger.log(request)
    request = JSON.parse(request);
    let messages, topic, queue;
    try {
        switch (request.headers.requestType) {
            case requestType.register:
                await UserDbLayer.register(request.body.data)
                acknowledgeClient(socket, request.headers.requestType, responseMessage.register)
                break
            case requestType.logIn:
                await UserDbLayer.logIn(request.body.data)
                acknowledgeClient(socket, request.headers.requestType, responseMessage.login)
                break
            case requestType.topic:
                let topics = await TopicDbLayer.getAllTopics()
                socket.write(ResponseBuilder.buildJSONResponse(request.headers.requestType, topics))
                break
            case requestType.pull:
                topic = await TopicDbLayer.getTopic(request.body.data.topic)
                RequestValidator.validatePullRequest(topic, request.body.data.user)
                let lastReadMessageIndex = QueueTopicSync.getLastReadMessageIndex(topic, request.body.data.user)
                if (!QueueHandler.isQueueExists(request.body.data.topic))
                    QueueHandler.createQueue(request.body.data.topic, await MessageDbLayer.getMessages(request.body.data.topic))
                queue = QueueHandler.getQueue(request.body.data.topic)
                messages = QueueMessageSync.pull(queue.queue, lastReadMessageIndex)
                await QueueTopicSync.incrementLastReadMessageIndex(request.body.data.topic, request.body.data.user, messages.length)
                socket.write(ResponseBuilder.buildJSONResponse(request.headers.requestType, messages))
                break
            case requestType.push:
                topic = await TopicDbLayer.getTopic(request.body.data.topic)
                RequestValidator.validatePushRequest(topic, request.body.data.user)
                if (!QueueHandler.isQueueExists(request.body.data.topic))
                    QueueHandler.createQueue(request.body.data.topic, await MessageDbLayer.getMessages(request.body.data.topic))
                queue = QueueHandler.getQueue(request.body.data.topic)
                QueueMessageSync.push(request, queue.queue)
                acknowledgeClient(socket, request.headers.requestType, responseMessage.put)
                break
            default:
                throw new RequestException.InvalidRequest()
        }
    } catch (error) {
        logger.error(error)
        socket.write(ResponseBuilder.buildJSONErrorResponse(request.headers.requestType, error.message))
    }
}

const errorEvent = (socket, error) => {
    logger.logClientInfo(socket);
    logger.error('Error : ' + error.message);
}

const endEvent = (socket) => {
    logger.logClientInfo(socket)
    let bytesRead = socket.bytesRead;
    let bytesWrite = socket.bytesWritten;
    logger.log('Bytes read : ' + bytesRead);
    logger.log('Bytes written : ' + bytesWrite);
    logger.log('Socket ended by Client!');
}

module.exports = {
    dataEvent,
    errorEvent,
    endEvent
}