const TopicDbLayer = require("../db/dbLayer/topicDbLayer")
const MessageDbLayer = require("../db/dbLayer/messageDbLayer")

class AdminService {
    static async addSubscriber(topic, user) {
        
        await TopicDbLayer.addSubscriber(topic, user)
    }

    static async addPublisher(topic, user) {
        await TopicDbLayer.addPublisher(topic, user)
    }

    static async createTopic(topic) {
        await TopicDbLayer.createTopic(topic)
        await MessageDbLayer.createMessageDocument(topic)
    }
}

module.exports = AdminService