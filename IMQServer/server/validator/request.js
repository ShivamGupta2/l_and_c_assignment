const RequestException = require("../exception/request")

class RequestValidator {
    static validatePullRequest(topic, user) {
        if (topic[0].subscribers.length == 0)
            throw new RequestException.NoSubscriber()
        let subscribers = topic[0].subscribers.map(subscriber => subscriber.userName)
        if (subscribers.indexOf(user) == -1)
            throw new RequestException.UserNotSubscribed()
    }

    static validatePushRequest(topic, user) {
        if (topic[0].publishers.length == 0)
            throw new RequestException.NoPublisher()
        if (topic[0].publishers.indexOf(user) == -1)
            throw new RequestException.UserNotPublisher()
    }
}

module.exports = RequestValidator