class Queue {
    constructor() {
        this.items = [];
    }

    enqueue(element) {
        this.items.push(element);
    }

    dequeue() {
        return this.isEmpty() ? "Underflow" : this.items.shift()
    }

    front() {
        return this.isEmpty() ? "No elements in Queue" : this.items[0]
    }

    isEmpty() {
        return this.items.length == 0;
    }

    peek() {
        if (this.isEmpty()) {
            return "Underflow";
        }
        return this.items[0];
    }

    printQueue() {
        var str = "";
        for (var i = 0; i < this.items.length; i++)
            str += this.items[i] + " ";
        return str;
    }

    pull(index) {
        return (this.isEmpty() || this.items.length < index) ? "Underflow" : this.items.slice(index)
    }

    size() {
        return this.items.length;
    }
}

module.exports = Queue