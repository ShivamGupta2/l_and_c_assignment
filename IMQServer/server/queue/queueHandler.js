const Queue = require("./queue")

class QueueHandler {
    static queues = []
    static createQueue(topic, messages) {
        if (!this.isQueueExists(topic)) {
            let queue = new Queue();
            messages[0].messages.map(message => queue.enqueue(message))
            this.queues.push({
                topic: messages[0].topic,
                queue
            })
        }
    }

    static isQueueExists(topic) {
        return (this.queues != undefined && this.queues.length != 0) ? this.queues.map(queue => queue.topic).includes(topic) : false
    }

    static getQueue(topic) {
        return this.queues[this.queues.map(queue => queue.topic).indexOf(topic)]
    }
}

module.exports = QueueHandler