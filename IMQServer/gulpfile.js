const gulp = require("gulp");
const terser = require('gulp-terser');

function json() {
    return gulp.src(["server/**/*.json", '!node_modules/**'])
        .pipe(gulp.dest("dist/"))
}

function js() {

    return gulp.src(['server/**/*.js', "!gulpfile.js", '!node_modules/**'])
        .pipe(terser())
        .pipe(gulp.dest("dist/"));
}
exports.default = gulp.series(json, js);